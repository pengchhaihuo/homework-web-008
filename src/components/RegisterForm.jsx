
import { Form, Button } from 'react-bootstrap'

import React, { Component } from 'react'
// import { FormGroup } from 'react-bootstrap';

export default class RegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            gender: "",
            email: "",
            password: ""
        }
    }

    getUsername = (e) => {
        this.setState({ username: e.target.value });
        console.log(this.state.username);
    }
    getEmail = (e) => {
        this.setState({ email: e.target.value });
        console.log(this.state.email);
    }
    getGender = (e) => {
        this.setState({ gender: e.target.value });
        console.log(this.state.gender);
    }
    getPassword = (e) => {
        this.setState({ password: e.target.value });
        console.log(this.state.password);
    }
    

    render() {
        return (
            <div>
                   
                <Form>
                    <Form.Group controlId="formBasic">
                        <Form.Label className="font-weight-bold">Username</Form.Label>
                        <Form.Control type="username" placeholder="Enter username" onChange={(e) => this.getUsername(e)} />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>
                    <Form.Group>
                        <p className="p-0 m-0 font-weight-bold">Gender</p>
                        <Form.Check custom inline label="male" type="radio" id="male" value="male" name="gender" onChange={(e) => this.getGender(e)} />
                        <Form.Check custom inline label="female" type="radio" id="female" value="female" name="gender"  onChange={(e) => this.getEmail(e)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label className="font-weight-bold">Email</Form.Label>
                        <Form.Control type="email" placeholder="abc@example.com" onChange={(e) => this.getEmail(e)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label className="font-weight-bold">Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={(e) => this.getPassword(e)} />
                    </Form.Group>

                    <Button variant="primary" type="submit" className={"rounded shadow"}  onClick ={() => this.props.addPerson(this.state)}>
                        Send</Button>
                </Form>
            </div >
        )
    }
}

