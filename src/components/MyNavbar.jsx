
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from 'react-bootstrap';
import React from 'react'


export default function MyNavbar() {
    return (
        <Navbar >
        <Navbar.Brand href="#home" ><span className="font-weight-bold">KSHRD Student</span></Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text >
            Signed in as: <a href="#login"  className="font-weight-bold">Admin</a>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
    )
}
